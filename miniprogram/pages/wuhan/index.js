// pages/wuhan/index.js
const plugin = requirePlugin("WechatSI")
const manager = plugin.getRecordRecognitionManager()
import std from '../../utils/std.js';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    topSwitch: false, //topBar开关
    top: 26, //topBar高度
    ttsSwitch: true,
    shareTotal: 0,
    page: 0,
    datePlaceholder: "日期 非必填",
    date: '',
    queryResult: [],
    valueData: {
      t_city: '',
      t_no: ''
    },
    busDataSwitch: {
      dbName: 'busData'
    },
    tipData: [{
      style: "grqj",
      title: '个人清洁',
      tips: ["经常保持双手清洁，尤其在触摸口、鼻或眼之前。", "经用洗手液和清水清洗双手，搓手最少20秒，并用纸巾擦干。", "打喷嚏或咳嗽时应用纸巾掩盖口鼻，把用过的纸巾弃置于有盖垃圾箱内，然后彻底清洁双手。"]
    }, {
      style: "jlbm",
      title: '尽量避免',
      tips: ["减少前往人流密集的场所。如不可避免，应戴上外科口罩。", "避免到访医院。如有必要到访医院，应佩戴外科口罩及时刻注重个人和手部卫生。", "避免接触动物（包括野味）、禽鸟或其粪便；避免到海鲜、活禽市场或农场。", "切勿进食野味及切勿光顾有提供野味的餐馆。", "注意食物安全和卫生，避免进食或饮用生或未熟透的动物产品，包括奶类、蛋类和肉类。"]
    }, {
      style: "jkjz",
      title: '尽快就诊',
      tips: ["如有身体不适，特别是有发烧或咳嗽，应戴上外科口罩并尽快就诊。"]
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const db = wx.cloud.database()

    // 获取分享人数
    this.getShareTotal();

    // 保存ttsSwitch设置
    wx.getStorage({
      key: 'ttsSwitch',
      success: (res) => {
        this.setData({
          ttsSwitch: res.data
        })
      },
      fail: (res) => {
        wx.setStorage({
          key: 'ttsSwitch',
          data: true,
        })
      }
    })

    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // 获取topBar高度
    this.setData({
      top: wx.getMenuButtonBoundingClientRect().top 
    })
  },

  onPageScroll(e) {
    // 获取屏幕滑动位置来控制topBar的开关
    if (e.scrollTop > 20) {
      this.setData({
        topSwitch:true
      })
    } else if (e.scrollTop < 20) {
      this.setData({
        topSwitch: false
      })
    }
  },
  //获取用户信息 
  onGotUserInfo: function(e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.userInfo)
    console.log(e.detail.rawData)
  },
  // 用户点击右上角分享
  share() {
    // 获取分享人数
    this.getShareTotal();
    var tts = "已有" + this.data.shareTotal + "人分享疫同程实时情报，感谢您的转发！武汉加油！"
    this.tts(tts)
  },
  onShareAppMessage: (res) => {
    const db = wx.cloud.database()
    if (res.from === 'button') {
      console.log("来自页面内转发按钮");
      console.log(res.target);
      //上传分享时间
      db.collection('share').add({
          data: {
            time: new Date()
          }
        })
        .then(res => {

        })

    } else {
      console.log("来自右上角转发菜单")
    }
    return {
      title: '武汉加油！2020加油！',
      path: '/pages/wuhan/index',
      imageUrl: "/images/share2.jpg",
      success: (res) => {
        console.log("转发成功", res);
      },
      fail: (res) => {
        console.log("转发失败", res);
      }
    }
  },
  //选择日期
  bindDateChange: function(e) {
    this.setData({
      date: e.detail.value
    })
  },
  //查询
  onQuery: function() {
    this.tts("正在查询")
    const db = wx.cloud.database()
    const _ = db.command
    // var dbName = this.data.busDataSwitch.dbName
    var dbName = "busData"
    var data = this.data.valueData
    //
    wx.showLoading({
      title: '查询中',
    })
    //模糊查询数据
    db.collection(dbName).where(
      _.or([{
          t_no: db.RegExp({
            regexp: data.t_no,
            options: 'i',
          }),
          t_date: db.RegExp({
            regexp: this.data.date,
            options: 'i',
          }),
          t_pos_start: db.RegExp({
            regexp: data.t_city,
            options: 'i',
          }),
        },
        {
          t_no: db.RegExp({
            regexp: data.t_no,
            options: 'i',
          }),
          t_date: db.RegExp({
            regexp: this.data.date,
            options: 'i',
          }),
          t_pos_end: db.RegExp({
            regexp: data.t_city,
            options: 'i',
          }),
        }
      ])
    ).skip(this.data.page).get({
      success: res => {
        wx.hideLoading();
        if (res.data.length == 0) {
          wx.showToast({
            icon: 'none',
            title: '暂无数据'
          })
        } else {
          this.setData({
            queryResult: res.data,
            page: this.data.page + 20
          })
        }
      },
      fail: err => {
        wx.hideLoading();
        wx.showToast({
          icon: 'none',
          title: '暂无数据'
        })
      }
    })
  },
  // 翻页查询
  pageQuery: function(data) {
    this.tts("加载中")
    const db = wx.cloud.database()
    const _ = db.command
    // var dbName = this.data.busDataSwitch.dbName
    var dbName = "busData"
    var data = this.data.valueData
    db.collection(dbName).where(
      _.or([{
          t_no: db.RegExp({
            regexp: data.t_no,
            options: 'i',
          }),
          t_date: db.RegExp({
            regexp: this.data.date,
            options: 'i',
          }),
          t_pos_start: db.RegExp({
            regexp: data.t_city,
            options: 'i',
          }),
        },
        {
          t_no: db.RegExp({
            regexp: data.t_no,
            options: 'i',
          }),
          t_date: db.RegExp({
            regexp: this.data.date,
            options: 'i',
          }),
          t_pos_end: db.RegExp({
            regexp: data.t_city,
            options: 'i',
          }),
        }
      ])
    ).skip(this.data.page).get({
      success: res => {
        let new_data = res.data;
        let old_data = this.data.queryResult;
        this.setData({
          queryResult: old_data.concat(new_data),
          //查询成功后page+20
          page: this.data.page + 20
        })
      },
      fail: err => {
      }
    })
  },
  //页面上拉触底事件的处理函数
  onReachBottom() {
    //判断是否已经查询过，如果没有查询过则会触发上拉触底事件
    if (this.data.valueData.t_city == '' && this.data.valueData.t_no == '') {} else {
      //判断查询到的结果的长度是否会被20整除 如果不能则说明已经加载完数据 如果可以则继续查询
      if (this.data.queryResult.length % 20 == 0) {
        this.pageQuery();
        console.log("页面上拉触底事件的处理函数")
      }
    }
  },
  // 提交表单
  formSubmit: function(e) {
    var that = this;
    //初始化键
    that.setData({
      valueData: e.detail.value,
      page: 0
    })
    var data = e.detail.value
    // 判断是否为空
    if (e.detail.value.t_city == "" && e.detail.value.t_no == "") {
      wx.showToast({
        icon: 'none',
        title: '请输入'
      })
      this.tts("请完善输入")
    } else {
      that.onQuery();
    }
  },
  // 重置input
  formReset: function() {
    this.tts("已重置输入")
    if (this.data.queryResult !== '') {
      this.setData({
        queryResult: [],
        value: [],
        valueData: {
          t_city: '',
          t_no: ''
        },
        date: ''
      })
    }
  },
  // 复制来源连接
  copyLink(e) {
    var id = e.target.id
    wx.setClipboardData({
      data: this.data.queryResult[id].source,
      success(res) {
        wx.vibrateShort()
        this.tts("已复制")
      }
    })
  },
  //获取分享人数
  getShareTotal() {
    const db = wx.cloud.database()
    db.collection("share").where({}).count().then(res => {
      console.log(res.total)
      this.setData({
        shareTotal: res.total
      })
    })
  },
  naviToPage(e) {
    var url = "/pages/xiansuo/index"
    this.tts("前往提供线索页面")
    wx.navigateTo({
      url
    })
  },
  //前往详情页面
  goTotrip(e) {
    console.log(e)
    this.tts("查看详情")
    var id = e.currentTarget.id
    var url = `/pages/trip/index?id=${id}`
    console.log(url)
    wx.navigateTo({
      url,
    })
  },
  // tts
  tts(e) {
    //如果开启语音辅助播报
    if (this.data.ttsSwitch) {
      // 播报
      std.Voice(e)
    }
  },
  focusCity(e) {
    this.tts("请输入相关地区 如：武汉")
  },
  focusNo(e) {
    this.tts("请输入车次或航班 如：G2020")
  },
  focusDate(e) {
    this.tts("请选择相关日期 非必填")
  },
  ttsSwitch() {
    if (this.data.ttsSwitch) {
      this.tts("语音辅助播报已关闭")
    } else {
      std.Voice("语音辅助播报已打开")
    }
    //将语音辅助播报开关存储到Data
    this.setData({
      ttsSwitch: !this.data.ttsSwitch
    })
    //将语音辅助播报开关存储到本地缓存
    wx.setStorage({
      key: 'ttsSwitch',
      data: this.data.ttsSwitch,
    })
  },
  //播报tips
  tip(e) {
    this.tts(e.currentTarget.dataset.name)
  },
   //播报tipTitle
  tipTitle(e) {
    this.tts(e.currentTarget.dataset.name)
  }
})